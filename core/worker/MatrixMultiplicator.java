package com.zahorodnii.matrix_multiplication.core.worker;

import com.zahorodnii.matrix_multiplication.core.entity.Matrix;

/**
 * @author Vladyslav_Zahorodnii
 * @version 1.0
 */
public interface MatrixMultiplicator {
    Matrix multiply();
}
