package com.zahorodnii.matrix_multiplication.core.worker.implementation;

import com.zahorodnii.matrix_multiplication.core.entity.Matrix;
import com.zahorodnii.matrix_multiplication.core.manipulator.MatrixManipulator;
import com.zahorodnii.matrix_multiplication.core.worker.MatrixMultiplicator;

import java.util.concurrent.RecursiveTask;

/**
 * @author Vladyslav_Zahorodnii
 * @version 1.0
 */
public class StrassenAlgorithm extends RecursiveTask<double[][]> implements MatrixMultiplicator {
    private int size;
    private Matrix factorMatrix;
    private Matrix multiplierMatrix;

    public StrassenAlgorithm(Matrix factorMatrix, Matrix multiplierMatrix, int size) {
        this.size = size;
        this.factorMatrix = factorMatrix;
        this.multiplierMatrix = multiplierMatrix;
    }

    @Override
    public Matrix multiply() {
        return new Matrix(compute());
    }

    @Override
    protected double[][] compute() {
        double[][] factorData = factorMatrix.getMatrixData();
        double[][] multiplierData = multiplierMatrix.getMatrixData();

        if (size <= 128) {
            multiplyTransposed(factorData, multiplierData);
        } else {
            size >>= 1;
        }


        double[][] a11 = new double[size][size];
        double[][] a12 = new double[size][size];
        double[][] a21 = new double[size][size];
        double[][] a22 = new double[size][size];

        double[][] b11 = new double[size][size];
        double[][] b12 = new double[size][size];
        double[][] b21 = new double[size][size];
        double[][] b22 = new double[size][size];

        splitMatrix(factorData, a11, a12, a21, a22);
        splitMatrix(multiplierData, b11, b12, b21, b22);

        StrassenAlgorithm task1 = new StrassenAlgorithm(MatrixManipulator.summation(new Matrix(a11), new Matrix(a22)), MatrixManipulator.summation(new Matrix(b11), new Matrix(b22)), size);
        StrassenAlgorithm task2 = new StrassenAlgorithm(MatrixManipulator.summation(new Matrix(a21), new Matrix(a22)), new Matrix(b11), size);
        StrassenAlgorithm task3 = new StrassenAlgorithm(new Matrix(a11), MatrixManipulator.subtraction(new Matrix(b12), new Matrix(b22)), size);
        StrassenAlgorithm task4 = new StrassenAlgorithm(new Matrix(a22), MatrixManipulator.subtraction(new Matrix(b21),new Matrix(b11)), size);
        StrassenAlgorithm task5 = new StrassenAlgorithm(MatrixManipulator.summation(new Matrix(a11), new Matrix(a12)), new Matrix(b22), size);
        StrassenAlgorithm task6 = new StrassenAlgorithm(MatrixManipulator.subtraction(new Matrix(a21), new Matrix(a11)), MatrixManipulator.summation(new Matrix(b11), new Matrix(b12)), size);
        StrassenAlgorithm task7 = new StrassenAlgorithm(MatrixManipulator.subtraction(new Matrix(a12), new Matrix(a22)), MatrixManipulator.summation(new Matrix(b21), new Matrix(b22)), size);

        task1.fork();
        task2.fork();
        task3.fork();
        task4.fork();
        task5.fork();
        task6.fork();
        task7.fork();

        double[][] p1 = task1.join();
        double[][] p2 = task2.join();
        double[][] p3 = task3.join();
        double[][] p4 = task4.join();
        double[][] p5 = task5.join();
        double[][] p6 = task6.join();
        double[][] p7 = task7.join();

        double[][] c11 = MatrixManipulator.summation(MatrixManipulator.summation(new Matrix(p1), new Matrix(p4)), MatrixManipulator.subtraction(new Matrix(p7), new Matrix(p5))).getMatrixData();
        double[][] c12 = MatrixManipulator.summation(new Matrix(p3), new Matrix(p5)).getMatrixData();
        double[][] c21 = MatrixManipulator.summation(new Matrix(p2), new Matrix(p4)).getMatrixData();
        double[][] c22 = MatrixManipulator.summation(MatrixManipulator.subtraction(new Matrix(p1), new Matrix(p2)), MatrixManipulator.summation(new Matrix(p3), new Matrix(p6))).getMatrixData();

        return collectMatrix(c11, c12, c21, c22);
    }

    public double[][] multiplyTransposed(double[][] a, double[][] b) {
        int rowsA = a.length;
        int columnsB = b[0].length;
        int columnsA_rowsB = a[0].length;

        double columnB[] = new double[columnsA_rowsB];
        double[][] c = new double[rowsA][columnsB];


        for (int j = 0; j < columnsB; j++) {
            for (int k = 0; k < columnsA_rowsB; k++) {
                columnB[k] = b[k][j];
            }

            for (int i = 0; i < rowsA; i++) {
                double rowA[] = a[i];
                int sum = 0;
                for (int k = 0; k < columnsA_rowsB; k++) {
                    sum += rowA[k] * columnB[k];
                }
                c[i][j] = sum;
            }
        }

        return c;
    }

    private void splitMatrix(double[][] splitMatrix, double[][] data11, double[][] data12, double[][] data21, double[][] data22) {
        int n = splitMatrix.length >> 1;

        for (int i = 0; i < n; i++) {
            System.arraycopy(splitMatrix[i], 0, data11[i], 0, n);
            System.arraycopy(splitMatrix[i], n, data12[i], 0, n);
            System.arraycopy(splitMatrix[i + n], 0, data21[i], 0, n);
            System.arraycopy(splitMatrix[i + n], n, data22[i], 0, n);
        }
    }

    private double[][] collectMatrix(double[][] data11, double[][] data12, double[][] data21, double[][] data22) {
        int size = data11.length;
        double[][] a = new double[size << 1][size << 1];

        for (int i = 0; i < size; i++) {
            System.arraycopy(data11[i], 0, a[i], 0, size);
            System.arraycopy(data12[i], 0, a[i], size, size);
            System.arraycopy(data22[i], 0, a[i + size], size, size);
        }

        return a;
    }
}
