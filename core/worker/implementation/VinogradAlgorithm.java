package com.zahorodnii.matrix_multiplication.core.worker.implementation;

import com.zahorodnii.matrix_multiplication.core.entity.Matrix;
import com.zahorodnii.matrix_multiplication.core.worker.MatrixMultiplicator;

/**
 * @author Vladyslav_Zahorodnii
 * @version 1.0
 */
public class VinogradAlgorithm implements MatrixMultiplicator {
    private Matrix factorMatrix;
    private Matrix multiplierMatrix;

    public VinogradAlgorithm(Matrix factorMatrix, Matrix multiplierMatrix) {
        this.factorMatrix = factorMatrix;
        this.multiplierMatrix = multiplierMatrix;
    }

    @Override
    public Matrix multiply() {
        double[][] factorData = factorMatrix.getMatrixData();
        double[][] multiplierData = multiplierMatrix.getMatrixData();
        double[][] resultData = new double[factorMatrix.getRows()][factorMatrix.getColumns()];

        for (int i = 0; i < factorMatrix.getRows(); i++) {
            for (int j = 0; j < multiplierMatrix.getColumns(); j++) {
                for (int k = 0; k < factorMatrix.getColumns(); k++) {
                    resultData[i][j] += factorData[i][k] * multiplierData[k][j];
                }
            }
        }

        return new Matrix(resultData);
    }
}
