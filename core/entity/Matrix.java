package com.zahorodnii.matrix_multiplication.core.entity;

import com.zahorodnii.matrix_multiplication.core.manipulator.MatrixManipulator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Arrays;
import java.util.Random;

/**
 * @author Vladyslav_Zahorodnii
 * @version 1.0
 */
@ToString
@EqualsAndHashCode
public class Matrix {
    private double[][] data;

    public Matrix(int rowsNumber, int columnsNumber) {
        data = new double[rowsNumber][columnsNumber];
    }

    public Matrix(double[][] matrixData) {
        data = matrixData;
    }

    public int getRows() {
        return data.length;
    }

    public int getColumns() {
        return data[0].length;
    }

    public double[][] getMatrixData() {
        return data;
    }

    public boolean isSquareMatrix() {
        return getRows() == getColumns();
    }

    public Matrix mulitply(Matrix multiplierMatrix) {
        return MatrixManipulator.multiplication(this, multiplierMatrix);
    }

    public void randomFill() {
        Random random = new Random();

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                data[i][j] = (int)(Math.random()*10);
            }
        }
    }
}
