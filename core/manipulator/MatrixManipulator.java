package com.zahorodnii.matrix_multiplication.core.manipulator;

import com.zahorodnii.matrix_multiplication.core.entity.Matrix;
import com.zahorodnii.matrix_multiplication.core.worker.implementation.StrassenAlgorithm;
import com.zahorodnii.matrix_multiplication.core.worker.implementation.VinogradAlgorithm;

/**
 * @author Vladyslav_Zahorodnii
 * @version 1.0
 */
public class MatrixManipulator {
    public static Matrix multiplication(Matrix factorMatrix, Matrix multiplierMatrix) {
        if (factorMatrix.isSquareMatrix() && multiplierMatrix.isSquareMatrix()) {
            if (factorMatrix.getRows() == multiplierMatrix.getRows() && factorMatrix.getRows() >= 64) {
                return new StrassenAlgorithm(factorMatrix, multiplierMatrix, factorMatrix.getRows()).multiply();
            }
        }

        return new VinogradAlgorithm(factorMatrix, multiplierMatrix).multiply();
    }

    public static Matrix summation(Matrix firstSummandMatrix, Matrix secondSummandMatrix) {
        double[][] firstSummandData = firstSummandMatrix.getMatrixData();
        double[][] secondSummandData = secondSummandMatrix.getMatrixData();
        double[][] resultData = new double[firstSummandMatrix.getRows()][firstSummandMatrix.getColumns()];

        for (int i = 0; i < firstSummandMatrix.getRows(); i++) {
            for (int j = 0; j < firstSummandMatrix.getColumns(); j++) {
                resultData[i][j] = firstSummandData[i][j] + secondSummandData[i][j];
            }
        }

        return new Matrix(resultData);
    }

    public static Matrix subtraction(Matrix reducedMatrix, Matrix subtractedMatrix) {
        double[][] reducedData = reducedMatrix.getMatrixData();
        double[][] subtractedData = subtractedMatrix.getMatrixData();
        double[][] resultData = new double[reducedMatrix.getRows()][reducedMatrix.getColumns()];

        for (int i = 0; i < reducedMatrix.getRows(); i++) {
            for (int j = 0; j < reducedMatrix.getColumns(); j++) {
                resultData[i][j] = reducedData[i][j] - subtractedData[i][j];
            }
        }

        return new Matrix(resultData);
    }
}
