package com.zahorodnii.matrix_multiplication.main;

import com.zahorodnii.matrix_multiplication.core.entity.Matrix;

/**
 * @author Vladyslav_Zahorodnii
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        Matrix firstMatrix = new Matrix(10, 10);
        Matrix secondMatrix = new Matrix(10, 10);

        firstMatrix.randomFill();
        secondMatrix.randomFill();

        Matrix resultMatrix = firstMatrix.mulitply(secondMatrix);
        System.out.println(resultMatrix);
    }
}
